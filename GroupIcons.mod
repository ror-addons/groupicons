<?xml version="1.0" encoding="UTF-8"?>
<!--
GroupIcons
Warhammer Online: Age of Reckoning UI modification that adds icons 
above party and warband members so it is easier to find your teammates 
in the thick of battle!
    
Copyright (C) 2008  Dillon "Rhekua" DeLoss
rhekua@msn.com		    www.rhekua.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Group Icons" version="1.3" date="7/9/2009" >
		<VersionSettings gameVersion="1.3.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Rhekua" email="rhekua@msn.com" />
		<Description text="Adds icons above party and warband members so it is easier to find your teammates in the thick of battle!" />
		<Files>
			<File name="GroupIcons.lua" />
			<File name="GroupIcons.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="GroupIcons.PREF_GROUP_ICON" />
			<SavedVariable name="GroupIcons.PREF_WARBAND_ICON" />
			<SavedVariable name="GroupIcons.PREF_LEADER_ICON" />
			<SavedVariable name="GroupIcons.PREF_ASSIST_ICON" />
			<SavedVariable name="GroupIcons.PREF_SCALE" />
		</SavedVariables>
		<OnInitialize>
			<CreateWindow name="GroupIconsWindow" show="true" />
			<CallFunction name="GroupIcons.OnInitialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="GroupIcons.OnUpdate" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="GroupIcons.OnShutdown" />
    		</OnShutdown>
	</UiMod>
</ModuleFile>
