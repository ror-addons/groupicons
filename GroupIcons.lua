--[[
GroupIcons
Warhammer Online: Age of Reckoning UI modification that adds icons 
above party and warband members so it is easier to find your teammates 
in the thick of battle!
    
Copyright (C) 2008-2009  Dillon "Rhekua" DeLoss
rhekua@msn.com		    www.rhekua.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]--

--[[ 
66
110
]]--

local TIME_DELAY = 1
local timeLeft = TIME_DELAY

GroupIcons = {}

GroupIcons.PREF_GROUP_ICON = 1
GroupIcons.PREF_WARBAND_ICON = 1
GroupIcons.PREF_LEADER_ICON = 1
GroupIcons.PREF_ASSIST_ICON = 1
GroupIcons.PREF_SCALE = 1

GroupIcons.HEALTH_HIGH_TINT = { r=0, g=255, b=0 }
GroupIcons.HEALTH_MED_TINT = { r=255, g=255, b=0 }
GroupIcons.HEALTH_LOW_TINT = { r=255, g=0, b=0 }

GroupIcons.HEALTH_HIGH_THRESHOLD = 100
GroupIcons.HEALTH_MED_THRESHOLD = 50
GroupIcons.HEALTH_LOW_THRESHOLD = 50

GroupIcons.ICON_GROUP = 66
GroupIcons.ICON_GROUP_DEAD = 68
GroupIcons.ICON_BATTLEGROUP = 110

GroupIcons.ICON_ARCHETYPE = { 2559, 2518, 2620, 2656 }

GroupIcons.NUM_WINDOWS = 24

GroupIcons.ICON_OFFSET_X = -5
GroupIcons.ICON_OFFSET_Y = 25

GroupIcons.LEADER_ICON_OFFSET_X = -1
GroupIcons.LEADER_ICON_OFFSET_Y = 3

GroupIcons.ASSIST_ICON_OFFSET_X = -12
GroupIcons.ASSIST_ICON_OFFSET_Y = -12

function GroupIcons.OnInitialize()
	for i = 1, GroupIcons.NUM_WINDOWS do
		_G["GroupIconWindow" .. i] = {}
		CreateWindowFromTemplate("GroupIconWindow" .. i, "GroupIconWindow", "Root")
		WindowSetShowing("GroupIconWindow" .. i, true)
		WindowSetAlpha("GroupIconWindow" .. i, 0)
		--AttachWindowToWorldObject("GroupIconWindow" .. i, GameData.Player.worldObjNum)
		DynamicImageSetTexture("GroupIconWindow" .. i .. "Icon", GetIconData(GroupIcons.ICON_GROUP), 0, 0)
		DynamicImageSetTexture("GroupIconWindow" .. i .. "Border", GetIconData(4), 0, 0)
		CircleImageSetTexture("GroupIconWindow" .. i .. "HealthBorder", GetIconData(398), 33, 33)
		WindowSetScale("GroupIconWindow" .. i .. "Icon", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
		WindowSetScale("GroupIconWindow" .. i .. "Border", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
		WindowSetScale("GroupIconWindow" .. i .. "HealthBorder", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
		WindowSetScale("GroupIconWindow" .. i .. "LeaderIcon", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
		WindowSetScale("GroupIconWindow" .. i .. "AssistIcon", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
		WindowSetShowing("GroupIconWindow" .. i .. "Icon", false)

		WindowClearAnchors("GroupIconWindow" .. i .. "LeaderIcon")
		WindowAddAnchor("GroupIconWindow" .. i .. "LeaderIcon", 
					"top", 
					"GroupIconWindow" .. i .. "Border", 
					"bottom", 
					GroupIcons.LEADER_ICON_OFFSET_X*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE, 
					GroupIcons.LEADER_ICON_OFFSET_Y*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)

		WindowClearAnchors("GroupIconWindow" .. i .. "AssistIcon")
		WindowAddAnchor("GroupIconWindow" .. i .. "AssistIcon", 
					"bottomright", 
					"GroupIconWindow" .. i .. "Border", 
					"topleft", 
					GroupIcons.ASSIST_ICON_OFFSET_X*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE, 
					GroupIcons.ASSIST_ICON_OFFSET_Y*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)

		WindowSetShowing("GroupIconWindow" .. i .. "LeaderIcon", false)
		WindowSetShowing("GroupIconWindow" .. i .. "AssistIcon", false)
		--WindowSetOffsetFromParent("GroupIconWindow" .. i .. "HealthBorder", GroupIcons.ICON_OFFSET_X, GroupIcons.ICON_OFFSET_Y)
	end
	if ( LibSlash ~= nil ) then
		LibSlash.RegisterSlashCmd("groupicons", function(input) GroupIcons.HandleSlashCmd(input) end)
	end
end

function GroupIcons.OnShutdown()
	for i = 1, GroupIcons.NUM_WINDOWS do
		DestroyWindow("GroupIconWindow" .. i)
	end
end

function GroupIcons.inGroup()
	return GroupWindow.groupData[1].name ~= L"" and GroupWindow.groupData[1].name ~= nil
end

function GroupIcons.inBattlegroup()
	if ( BattlegroupWindow ~= nil ) then
		return BattlegroupWindow.IsBattlegroupActive()
	elseif ( BattlegroupHUD ~= nil ) then
    		for i = 1, table.getn(GetBattlegroupMemberData()) do
        		if ( table.getn(GetBattlegroupMemberData()[i].players) > 0 ) then
            		return true
        		end
   		end
	end
end

function GroupIcons.HandleSlashCmd(arg)
	if ( arg == "party new" ) then
		GroupIcons.PREF_GROUP_ICON = 1
		return
	elseif ( arg == "party old" ) then
		GroupIcons.PREF_GROUP_ICON = 0
		return
	elseif ( arg == "group new" ) then
		GroupIcons.PREF_GROUP_ICON = 1
		return
	elseif ( arg == "group old" ) then
		GroupIcons.PREF_GROUP_ICON = 0
		return
	elseif ( arg == "warband new" ) then
		GroupIcons.PREF_WARBAND_ICON = 1
		return
	elseif ( arg == "warband old" ) then
		GroupIcons.PREF_WARBAND_ICON = 0
		return
	elseif ( arg == "leader show" ) then
		GroupIcons.PREF_LEADER_ICON = 1
		return
	elseif ( arg == "leader hide" ) then
		GroupIcons.PREF_LEADER_ICON = 0
		return
	elseif ( arg == "assist show" ) then
		GroupIcons.PREF_ASSIST_ICON = 1
		return
	elseif ( arg == "assist hide" ) then
		GroupIcons.PREF_ASSIST_ICON = 0
		return
	elseif ( string.find(arg, "scale ") ~= nil ) then
		local tempString = string.sub(arg, 7, -1)
		if ( tempString ~= "" and tonumber(tempString) ~= nil ) then
			GroupIcons.PREF_SCALE = tonumber(tempString)
			for i = 1, GroupIcons.NUM_WINDOWS do
				WindowSetScale("GroupIconWindow" .. i .. "Icon", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
				WindowSetScale("GroupIconWindow" .. i .. "Border", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
				WindowSetScale("GroupIconWindow" .. i .. "HealthBorder", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
				WindowSetScale("GroupIconWindow" .. i .. "LeaderIcon", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)
				WindowSetScale("GroupIconWindow" .. i .. "AssistIcon", InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)

				WindowClearAnchors("GroupIconWindow" .. i .. "LeaderIcon")
				WindowAddAnchor("GroupIconWindow" .. i .. "LeaderIcon", 
							"top", 
							"GroupIconWindow" .. i .. "Border", 
							"bottom", 
							GroupIcons.LEADER_ICON_OFFSET_X*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE, 
							GroupIcons.LEADER_ICON_OFFSET_Y*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)

				WindowClearAnchors("GroupIconWindow" .. i .. "AssistIcon")
				WindowAddAnchor("GroupIconWindow" .. i .. "AssistIcon", 
							"bottomright", 
							"GroupIconWindow" .. i .. "Border", 
							"topleft", 
							GroupIcons.ASSIST_ICON_OFFSET_X*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE, 
							GroupIcons.ASSIST_ICON_OFFSET_Y*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)

				--WindowSetOffsetFromParent("GroupIconWindow" .. i .. "HealthBorder", GroupIcons.ICON_OFFSET_X, GroupIcons.ICON_OFFSET_Y)
			end
		else
			EA_ChatWindow.Print(L"Invalid number for: /groupicons [scale] [number]")
		end
		return
	end
	EA_ChatWindow.Print(L"Usage: /groupicons [party/warband] [new/old]\n -- Sets the party or warband icons to the new or old versions.\nExample: /groupicons party new\n -- Sets party icons to the new version."
			     .. L"\nUsage: /groupicons [leader/assist] [show/hide]\n -- Shows or hides leader or assist icons." 
			     .. L"\nUsage: /groupicons [scale] [number]\n -- Sets the scale of the icons.\nExample: /groupicons scale 0.5\n -- Sets the icon scale to half-size." )
end

function GroupIcons.HandleIconPreference( windowName )
	if ( _G[windowName].inGroup ) then
		if ( GroupIcons.PREF_GROUP_ICON == 0 ) then
			WindowSetShowing(windowName .. "ClassIcon",  false)
			WindowSetShowing(windowName .. "Border",  false)
			WindowSetShowing(windowName .. "HealthBorder",  false)
			WindowSetShowing(windowName .. "Icon", true)
		elseif ( GroupIcons.PREF_GROUP_ICON == 1 ) then
			WindowSetShowing(windowName .. "ClassIcon",  true)
			WindowSetShowing(windowName .. "Border",  true)
			WindowSetShowing(windowName .. "HealthBorder",  true)
			WindowSetShowing(windowName .. "Icon", false)
		end
	else
		if ( GroupIcons.PREF_WARBAND_ICON == 0 ) then
			WindowSetShowing(windowName .. "ClassIcon",  false)
			WindowSetShowing(windowName .. "Border",  false)
			WindowSetShowing(windowName .. "HealthBorder",  false)
			WindowSetShowing(windowName .. "Icon", true)
		elseif ( GroupIcons.PREF_WARBAND_ICON == 1 ) then
			WindowSetShowing(windowName .. "ClassIcon",  true)
			WindowSetShowing(windowName .. "Border",  true)
			WindowSetShowing(windowName .. "HealthBorder",  true)
			WindowSetShowing(windowName .. "Icon", false)
		end
	end

	if ( _G[windowName].isGroupLeader ) then
		if ( GroupIcons.PREF_LEADER_ICON == 0 ) then
			WindowSetShowing(windowName .. "LeaderIcon",  false)
		elseif ( GroupIcons.PREF_LEADER_ICON == 1 ) then
			WindowSetShowing(windowName .. "LeaderIcon",  true)
		end
	else
		WindowSetShowing(windowName .. "LeaderIcon",  false)
	end

	if ( _G[windowName].isMainAssist ) then
		if ( GroupIcons.PREF_ASSIST_ICON == 0 ) then
			WindowSetShowing(windowName .. "AssistIcon",  false)
		elseif ( GroupIcons.PREF_ASSIST_ICON == 1 ) then
			WindowSetShowing(windowName .. "AssistIcon",  true)
		end
	else
		WindowSetShowing(windowName .. "AssistIcon",  false)
	end
end

function GroupIcons.GetGroup()
	local groupNumber = 0

	if ( GroupIcons.inBattlegroup() ) then
    		for i = 1, table.getn(GetBattlegroupMemberData()) do
			if ( GetBattlegroupMemberData()[i].players ~= nil ) then
					
				for j = 1, table.getn(GetBattlegroupMemberData()[i].players) do	

					if ( GetBattlegroupMemberData()[i].players[j].worldObjNum ~= 0 
						and GameData.Player.worldObjNum == GetBattlegroupMemberData()[i].players[j].worldObjNum ) then

						groupNumber = i
						break
					end
				end
			end
   		end
	end

	return groupNumber
end

function GroupIcons.GetArchetype( careerLine )
	-- tanks
	if ( careerLine == 1
		or careerLine == 17
		or careerLine == 13 
		or careerLine == 5
		or careerLine == 21
		or careerLine == 10 ) then

			return 1

	-- mdps
	elseif ( careerLine == 6
			or careerLine == 14
			or careerLine == 2
			or careerLine == 19
			or careerLine == 22
			or careerLine == 9 ) then

			return 2

	-- rdps
	elseif ( careerLine == 4
			or careerLine == 18
			or careerLine == 16
			or careerLine == 8
			or careerLine == 24
			or careerLine == 11 ) then

			return 3

	-- healer
	elseif ( careerLine == 20
			or careerLine == 23
			or careerLine == 3
			or careerLine == 7
			or careerLine == 12
			or careerLine == 15 ) then

			return 4

	end
end

function GroupIcons.GetHealthColor( percent )
	local color = { r=0, g=0, b=0 }

	local weight = 0

	if ( percent == 0 ) then
		color.r = 125
		color.g = 125
		color.b = 125
		return color
	end

	weight = ( percent - GroupIcons.HEALTH_MED_THRESHOLD ) / GroupIcons.HEALTH_MED_THRESHOLD
	if ( weight > 0 ) then
		color.r = color.r + ( GroupIcons.HEALTH_HIGH_TINT.r * weight )
		color.g = color.g + ( GroupIcons.HEALTH_HIGH_TINT.g * weight )
		color.b = color.b + ( GroupIcons.HEALTH_HIGH_TINT.b * weight )
	end

	weight = ( percent ) / GroupIcons.HEALTH_MED_THRESHOLD
	if ( weight > 1 ) then
		weight = 2 - weight
	end
	color.r = color.r + ( GroupIcons.HEALTH_MED_TINT.r * weight )
	color.g = color.g + ( GroupIcons.HEALTH_MED_TINT.g * weight )
	color.b = color.b + ( GroupIcons.HEALTH_MED_TINT.b * weight )

	weight = ( GroupIcons.HEALTH_LOW_THRESHOLD - percent ) / GroupIcons.HEALTH_LOW_THRESHOLD
	if ( weight <= 1 and weight > 0 ) then
		color.r = color.r + ( GroupIcons.HEALTH_LOW_TINT.r * weight )
		color.g = color.g + ( GroupIcons.HEALTH_LOW_TINT.g * weight )
		color.b = color.b + ( GroupIcons.HEALTH_LOW_TINT.b * weight )
	end

	return color
end

function GroupIcons.SetWindowAlphaByMousePosition( window, maxDist )
	local windowX, windowY = WindowGetScreenPosition( window )

	local distance = math.sqrt( (windowX - SystemData.MousePosition.x)^2 + (windowY - SystemData.MousePosition.y)^2 )

	if ( distance < maxDist ) then
		WindowSetAlpha( window,((distance-30)/(maxDist)) )
	else
		WindowSetAlpha( window, 1 )
	end
end

function GroupIcons.GetWindowAlphaByWindowPosition( window, otherWindow, maxDist )
	local windowX, windowY = WindowGetScreenPosition( window )
	local otherWindowX, otherWindowY = WindowGetScreenPosition( otherWindow )

	local distance = math.sqrt( (windowX - otherWindowX)^2 + (windowY - otherWindowY)^2 )

	if ( distance < maxDist ) then
		local alpha = ((distance-5)/(maxDist))
		return alpha+0.01
	else
		return 1
	end
end

function GroupIcons.Icon_OnLButtonDown( flags )
	local windowName = SystemData.ActiveWindow.name
 	local shiftPressed = (flags == SystemData.ButtonFlags.SHIFT); 
end

function GroupIcons.Icon_OnMouseOver()
	local windowName = SystemData.ActiveWindow.name

end

function GroupIcons.Icon_OnUpdate()
	local windowName = SystemData.ActiveWindow.name
	GroupIcons.SetWindowAlphaByMousePosition(windowName, 100 )
end

function GroupIcons.IconWindow_OnUpdate()
	local windowName = SystemData.ActiveWindow.name
	if ( not _G[windowName].inGroup and WindowGetAlpha(windowName) > 0 ) then
		local finalAlpha = 100
		for i = 1, GroupIcons.NUM_WINDOWS do
			if ( _G["GroupIconWindow" .. i].inGroup and WindowGetAlpha("GroupIconWindow" .. i) > 0 ) then
				local alpha = GroupIcons.GetWindowAlphaByWindowPosition( windowName, "GroupIconWindow" .. i, 46 )
				if ( alpha < finalAlpha ) then
					finalAlpha = alpha
				end
			end
		end
		WindowSetAlpha( windowName, finalAlpha )
	end
end

function GroupIcons.OnUpdate( elapsed )

	timeLeft = timeLeft - elapsed
	if timeLeft > 0 then
		return -- cut out early
	end
	timeLeft = TIME_DELAY -- reset to TIME_DELAY seconds
	-- this will run roughly every second. If TIME_DELAY were 2, it'd run every 2 seconds.

	for i = 1, GroupIcons.NUM_WINDOWS do
		WindowSetAlpha("GroupIconWindow" .. i, 0)
		GroupIcons.HandleIconPreference( "GroupIconWindow" .. i )
	end

	if ( GroupIcons.inBattlegroup() ) then
		local ourGroup = GroupIcons.GetGroup()
		local workingNumber = 1
    		for i = 1, table.getn(GetBattlegroupMemberData()) do

			if ( GetBattlegroupMemberData()[i].players ~= nil ) then
					
				for j = 1, table.getn(GetBattlegroupMemberData()[i].players) do	

					if ( GetBattlegroupMemberData()[i].players[j].worldObjNum ~= 0
						and GetBattlegroupMemberData()[i].players[j].worldObjNum ~= GameData.Player.worldObjNum ) then

						WindowSetAlpha("GroupIconWindow" .. workingNumber, 1)

						if ( _G["GroupIconWindow" .. workingNumber].lastObjNum ~= GetBattlegroupMemberData()[i].players[j].worldObjNum ) then

							if ( _G["GroupIconWindow" .. workingNumber].lastObjNum ~= nil ) then
								DetachWindowFromWorldObject("GroupIconWindow" .. workingNumber, _G["GroupIconWindow" .. workingNumber].lastObjNum)
							end
							AttachWindowToWorldObject("GroupIconWindow" .. workingNumber, GetBattlegroupMemberData()[i].players[j].worldObjNum)
						end
						_G["GroupIconWindow" .. workingNumber].lastObjNum = GetBattlegroupMemberData()[i].players[j].worldObjNum

						--WindowSetOffsetFromParent("GroupIconWindow" .. i .. "HealthBorder", -SystemData.screenResolution.x/2, (-SystemData.screenResolution.y/2)+GroupIcons.ICON_OFFSET_Y)

						local inGroup = false
						if ( ourGroup == i ) then
							inGroup = true
						end
						_G["GroupIconWindow" .. workingNumber].inGroup = inGroup 

						if ( inGroup ) then
							DynamicImageSetTexture("GroupIconWindow" .. workingNumber .. "Icon", GetIconData(GroupIcons.ICON_GROUP), 0, 0)
							WindowSetTintColor("GroupIconWindow" .. workingNumber .. "Border", 125, 255, 125)
						else
							DynamicImageSetTexture("GroupIconWindow" .. workingNumber .. "Icon", GetIconData(GroupIcons.ICON_BATTLEGROUP), 0, 0)
							WindowSetTintColor("GroupIconWindow" .. workingNumber .. "Border", 125, 125, 255)
						end

						_G["GroupIconWindow" .. workingNumber].isGroupLeader = GetBattlegroupMemberData()[i].players[j].isGroupLeader 
						_G["GroupIconWindow" .. workingNumber].isMainAssist = GetBattlegroupMemberData()[i].players[j].isMainAssist 

						CircleImageSetTexture("GroupIconWindow" .. workingNumber .. "ClassIcon", 
							GetIconData(Icons.GetCareerIconIDFromCareerLine(GetBattlegroupMemberData()[i].players[j].careerLine)), 16, 16)

						local healthColor = GroupIcons.GetHealthColor( GetBattlegroupMemberData()[i].players[j].healthPercent )
						if ( inGroup ) then
							WindowSetTintColor("GroupIconWindow" .. workingNumber .. "HealthBorder", healthColor.r, healthColor.g, healthColor.b)
						else
							WindowSetTintColor("GroupIconWindow" .. workingNumber .. "HealthBorder", healthColor.r/2, healthColor.g/2, healthColor.b/2)
						end
						WindowSetScale("GroupIconWindow" .. workingNumber .. "ClassIcon", (0.80+(GetBattlegroupMemberData()[i].players[j].healthPercent/100*0.20))*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)

						if ( GetBattlegroupMemberData()[i].players[j].healthPercent == 0 ) then
							WindowSetTintColor("GroupIconWindow" .. workingNumber .. "Icon", 255, 0, 0)
							if ( inGroup ) then
								DynamicImageSetTexture("GroupIconWindow" .. workingNumber .. "Icon", GetIconData(GroupIcons.ICON_GROUP_DEAD), 0, 0)
								WindowSetTintColor("GroupIconWindow" .. workingNumber .. "ClassIcon", 255, 0, 0)
							else
								WindowSetTintColor("GroupIconWindow" .. workingNumber .. "ClassIcon", 125, 0, 0)
							end
						else
							WindowSetTintColor("GroupIconWindow" .. workingNumber .. "Icon", 255, 255, 255)
							if ( inGroup ) then
								WindowSetTintColor("GroupIconWindow" .. workingNumber .. "ClassIcon", 255, 255, 255)
							else
								WindowSetTintColor("GroupIconWindow" .. workingNumber .. "ClassIcon", 125, 125, 125)
							end
						end
						
						--WindowSetOffsetFromParent("GroupIconWindow" .. workingNumber .. "Icon", GroupIcons.ICON_OFFSET_X, GroupIcons.ICON_OFFSET_Y)
					else
						WindowSetAlpha("GroupIconWindow" .. workingNumber, 0)
					end
					workingNumber = workingNumber + 1
				end
			end
   		end
	elseif ( GroupIcons.inGroup() ) then
		local workingNumber = 1
		for i = 1, table.getn(GetGroupData()) do
			if ( GetGroupData()[i].worldObjNum ~= 0  ) then

				WindowSetAlpha("GroupIconWindow" .. workingNumber, 1)

				if ( _G["GroupIconWindow" .. workingNumber].lastObjNum ~= GetGroupData()[i].worldObjNum ) then

					if ( _G["GroupIconWindow" .. workingNumber].lastObjNum ~= nil ) then
						DetachWindowFromWorldObject("GroupIconWindow" .. workingNumber, _G["GroupIconWindow" .. workingNumber].lastObjNum)
					end
					AttachWindowToWorldObject("GroupIconWindow" .. workingNumber, GetGroupData()[i].worldObjNum)
				end
				_G["GroupIconWindow" .. workingNumber].lastObjNum = GetGroupData()[i].worldObjNum

				_G["GroupIconWindow" .. workingNumber].inGroup = true 
				_G["GroupIconWindow" .. workingNumber].isGroupLeader = GetGroupData()[i].isGroupLeader 
				_G["GroupIconWindow" .. workingNumber].isMainAssist = GetGroupData()[i].isMainAssist 

				--WindowSetOffsetFromParent("GroupIconWindow" .. i .. "HealthBorder", -SystemData.screenResolution.x/2, (-SystemData.screenResolution.y/2)+GroupIcons.ICON_OFFSET_Y)

				DynamicImageSetTexture("GroupIconWindow" .. workingNumber .. "Icon", GetIconData(GroupIcons.ICON_GROUP), 0, 0)
				WindowSetTintColor("GroupIconWindow" .. workingNumber .. "Border", 125, 255, 125)

				CircleImageSetTexture("GroupIconWindow" .. workingNumber .. "ClassIcon", 
					GetIconData(Icons.GetCareerIconIDFromCareerLine(GetGroupData()[i].careerLine)), 16, 16)
						
				local healthColor = GroupIcons.GetHealthColor( GetGroupData()[i].healthPercent )
				WindowSetTintColor("GroupIconWindow" .. workingNumber .. "HealthBorder", healthColor.r, healthColor.g, healthColor.b)
				WindowSetScale("GroupIconWindow" .. workingNumber .. "ClassIcon", (0.80+(GetGroupData()[i].healthPercent/100*0.20))*InterfaceCore.GetScale()*GroupIcons.PREF_SCALE)

				if ( GetGroupData()[i].healthPercent == 0 ) then
					WindowSetTintColor("GroupIconWindow" .. workingNumber, 255, 0, 0)
					WindowSetTintColor("GroupIconWindow" .. workingNumber .. "ClassIcon", 255, 0, 0)
					DynamicImageSetTexture("GroupIconWindow" .. workingNumber .. "Icon", GetIconData(GroupIcons.ICON_GROUP_DEAD), 0, 0)
				else
					WindowSetTintColor("GroupIconWindow" .. workingNumber, 255, 255, 255)
					WindowSetTintColor("GroupIconWindow" .. workingNumber .. "ClassIcon", 255, 255, 255)
				end

				--WindowSetOffsetFromParent("GroupIconWindow" .. workingNumber .. "Icon", GroupIcons.ICON_OFFSET_X, GroupIcons.ICON_OFFSET_Y)
			else
				WindowSetAlpha("GroupIconWindow" .. workingNumber, 0)
			end
			workingNumber = workingNumber + 1
		end
	else
		for i = 1, GroupIcons.NUM_WINDOWS do
			WindowSetAlpha("GroupIconWindow" .. i, 0)
		end
		--WindowSetAlpha("GroupIconWindow" .. 1, 1)
		--AttachWindowToWorldObject("GroupIconWindow" .. 1, GameData.Player.worldObjNum)
	end
end